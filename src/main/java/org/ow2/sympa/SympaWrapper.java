/**
 This file is part of Sympa SOAP service Component.

 Sympa SOAP service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Sympa SOAP is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Sympa SOAP (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved

 @author Alexandre Lefebvre
 */
package org.ow2.sympa;

import org.xwiki.component.annotation.Role;

import java.util.Map;


/**
 * Interface of the Sympa SOAP wrapper Component
 */
@Role
public interface SympaWrapper {

    /**
     * Initialize the Sympa wrapper.
     * @param params Map containing values for sympaSoapUrl, trustedApp and trustedAppPassword (compulsory)
     *               If params contains a value for defaults, this should be a Map, with possible keys
     *               defaultTemplate, defaultTopic (for list creation) and
     *               defaultQuiet (for adding or removing users from lists).
     *               If present, those will replace the default values, respectively:
     *               - defaultTemplate: private_working_group
     *               - defaultTopic: computing
     *               - defaultQuiet: 1 (quiet mode)
     * @return a Map containing error and errorType, and result.
     */
    Map init(Map params);

    /**
     * Get the mailing lists to which the user is a subscriber, moderator or owner.
     *
     * @param user The user for which to test the list membership.
     * @return A Map containing error and errorType, and result which is a list of maps.
     * Each map contains items for a given list:
     * - isSubscriber (0 or 1)
     * - isOwner (0 or 1)
     * - isEditor (0 or 1) - whether the user is a moderator of the list
     * - subject (the list subject)
     * - listAddress (e.g. my-list@mydoma.in)
     * - name (e.g. my-list)
     * - homepage (web URL for the list)
     */
    Map getUserLists(String user);

    /**
     * Get a list of all mailing lists
     *
     * @param user The sympa user which runs the command.
     *             Note that different users could view different lists. A sympa admin would view all lists.
     * @return A Map containing error and errorType, and result which is a list of maps.
     * Each map contains items for a given list:
     * - subject (the list subject)
     * - listAddress (e.g. my-list@mydoma.in)
     * - name (e.g. my-list)
     * - homepage (web URL for the list)
     */
    Map getLists(String user);

    /**
     * Get information about a mailing list for a given user.
     *
     * @param params A Map containing two entries:
     *               userEmail: the user on behalf of which the Sympa info command is executed
     *               listName: the name of the mailing list for which to retrieve information
     * @return A Map containing error and errorType, and result which is a list of maps.
     * Each map contains items for a given list:
     * - isSubscriber (0 or 1)
     * - isOwner (0 or 1)
     * - isEditor (0 or 1) - whether the user is a moderator of the list
     * - subject (the list subject)
     * - listAddress (e.g. my-list@mydoma.in)
     * - homepage (web URL for the list)
     */
    Map getInfo(Map params);

    /**
     * Get the subscribers list of a mailing list for a given user.
     *
     * @param params A Map containing two entries:
     *               userEmail: the user on behalf of which the Sympa review command is executed
     *               listName: the name of the mailing list for which to retrieve information
     * @return A Map containing error and errorType, and result which is an array of subscribers.
     */
    Map getListSubscribers(Map params);

    /**
     * Create a mailing list.
     *
     * @param params A Map containing:
     *               - userEmail: the user requesting the list creation (mandatory)
     *               Note: if the user is a listmaster, the list will be activated,
     *               otherwise it will be pending.
     *               - listName: the name of the list to be created (mandatory)
     *               - subject: the subject of the list (mandatory)
     *               - description: the description of the list (mandatory)
     *               - topic: the topic of the list (optional, otherwise defaultTopic will be used)
     *               - template: the template for the list (optional, otherwise defaultTemplate will be used)
     * @return A Map containing error and errorType, and result which is either true or false
     */
    Map createList(Map params);

    /**
     * Add a user to a mailing list.
     *
     * @param params A Map containing:
     *               - userEmail: the user email to add as a list subscriber (mandatory)
     *               - sympaUserEmail: the user performing the request (mandatory)
     *               - listName: the name of the list to be created (mandatory)
     *               - userFullName (optional): the full name of the user. If not present, "" will be used.
     *               - quiet (optional): whether the subscription should be quiet or not.
     *               If not present, the defaultQuiet will be used.
     * @return A Map containing error and errorType, and result which is either true or false
     */
    Map addUserToList(Map params);

    /**
     * Remove a user from a mailing list.
     *
     * @param params A Map containing:
     *               - userEmail: the user email to add as a list subscriber (mandatory)
     *               - sympaUserEmail: the user performing the request (mandatory)
     *               - listName: the name of the list to be created (mandatory)
     *               - quiet (optional): whether the subscription should be quiet or not.
     *               If not present, the defaultQuiet will be used.
     * @return A Map containing error and errorType, and result which is either true or false
     */
    Map removeUserFromList(Map params);

    /**
     * Get information about a mailing list for a given user.
     *
     * @param params A Map containing two entries:
     *               userEmail: the user on behalf of which the Sympa info command is executed
     *               listName: the name of the mailing list for which to retrieve information
     *               role: the role to check (valid roles are subscriber, editor and owner)
     * @return A Map containing error and errorType, and result which is either true or false
     */
    Map amI(Map params);

    /**
     * Encrypt a String
     *
     * @param input : the String to be encrypted
     * @return the encrypted String
     */
    String encrypt(String input);

    /**
     * Decrypt a String
     * @param input : the String to be decrypted
     * @return the decrypted String
     */
    String decrypt(String input);

}

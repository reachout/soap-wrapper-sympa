/**
 This file is part of Sympa SOAP service Component.

 Sympa SOAP service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Sympa SOAP is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Sympa SOAP (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved

 @author Alexandre Lefebvre
 */
package org.ow2.sympa.internal;

import com.sun.org.apache.xml.internal.security.utils.Base64;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.ow2.sympa.SympaWrapper;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xwiki.component.annotation.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;

import javax.inject.Singleton;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Implementation of a <tt>SympaWrapper</tt> component.
 */
@Component
@Singleton
public class SympaWrapperImpl implements SympaWrapper {

    private String sympasoapUrl;
    private String trustedApp;
    private String appPwd;

    private String keyFileLocation;
    private Properties propertiesResource;
    private String md5;

    //default values for mailing lists topic, template and quiet parameters
    private String defaultTopic = "computing";
    private String defaultTemplate = "private_working_group";
    private String defaultQuiet = "1";

    private final static String ERRORSYMPA = "ErrorSympa";
    private final static String ERRORWRAPPER = "ErrorWrapper";
    private final static String PARAMERR_USEREMAIL = "Missing entry for userEmail in parameters";
    private final static String PARAMERR_LISTNAME = "Missing entry for listName in parameters";
    private final static String PARAMERR_SYMPASOAPURL = "Missing entry for sympaSoapUrl in parameters";
    private final static String PARAMERR_TRUSTEDAPP = "Missing entry for trustedApp in parameters";
    private final static String PARAMERR_APPPASSWORD = "Missing entry for appPassword in parameters";
    private final static String PARAMERR_ROLE = "Missing entry for role in parameters";
    private final static String PARAMERR_SUBJECT = "Missing entry for subject in parameters";
    private final static String PARAMERR_DESCRIPTION = "Missing entry for description in parameters";
    private final static String PARAMERR_SYMPAUSEREMAIL = "Missing entry for sympaUserEmail in parameters";
    private final static String PARAM_USEREMAIL = "userEmail";
    private final static String PARAM_LISTNAME = "listName";
    private final static String PARAM_SYMPASOAPURL = "sympaSoapUrl";
    private final static String PARAM_TRUSTEDAPP = "trustedApp";
    private final static String PARAM_APPPASSWORD = "appPassword";
    private final static String PARAM_SUBJECT = "subject";
    private final static String PARAM_DESCRIPTION = "description";
    private final static String PARAM_TEMPLATE = "template";
    private final static String PARAM_TOPIC = "topic";
    private final static String PARAM_QUIET = "quiet";
    private final static String PARAM_SYMPAUSEREMAIL = "sympaUserEmail";
    private final static String PARAM_USERFULLNAME = "userFullName";
    private final static String PARAM_DEF_TEMPLATE = "defaultTemplate";
    private final static String PARAM_DEF_QUIET = "defaultQuiet";
    private final static String PARAM_DEF_TOPIC = "defaultTopic";
    private final static String PARAM_ROLE = "role";

    private final static String ERRORTYPE = "errorType";
    private final static String ERROR = "error";
    private final static String RESULT = "result";


    private final static String soapAction = "urn:sympasoap#authenticateRemoteAppAndRun";
    private final static String soapEnvStart
            = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><soap:Envelope soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Body><authenticateRemoteAppAndRun xmlns=\"urn:sympasoap\"><c-gensym3 xsi:type=\"xsd:string\">";
    private final static String soapEnvAppToPwd = "</c-gensym3><c-gensym5 xsi:type=\"xsd:string\">";
    private final static String soapEnvPwdToUserEmail = "</c-gensym5><c-gensym7 xsi:type=\"xsd:string\">USER_EMAIL=";
    private final static String soapEnvUserEmailToMethod = "</c-gensym7><c-gensym9 xsi:type=\"xsd:string\">";
    private final static String soapEnvBeforeParam = "</c-gensym9><soapenc:Array soapenc:arrayType=\"xsd:anyType[";
    private final static String soapEnvNoParam = "]\" xsi:type=\"soapenc:Array\" />";
    private final static String soapEnvWithParam = "]\" xsi:type=\"soapenc:Array\">";
    private final static String soapEnvAfterParam = "</soapenc:Array>";
    private final static String soapEnvEnd = "</authenticateRemoteAppAndRun></soap:Body></soap:Envelope>";
    private final static String itemStart = "<item xsi:type=\"xsd:string\">";
    private final static String itemEnd = "</item>";

    /**
     * There should be on the filesystem:
     * 1. file /etc/ow2/wrappers/wrappers.conf containing a line KEYFILE_LOCATION=path_to_file
     * 2. file path_to_file should also exist
     */
    public SympaWrapperImpl() {
        try {
            com.sun.org.apache.xml.internal.security.Init.init();
            FileInputStream is = new FileInputStream(new File("/etc/ow2/wrappers/wrappers.conf"));
            propertiesResource = new Properties();
            propertiesResource.load(is);
            keyFileLocation = (String) propertiesResource.get("KEYFILE_LOCATION");
            FileInputStream keyfileio = new FileInputStream(new File(keyFileLocation));
            md5 = DigestUtils.md5Hex(keyfileio);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes the Sympa wrapper.
     *
     * @param params Map containing values for sympaSoapUrl, trustedApp and trustedAppPassword (compulsory)
     *               If params contains a value for defaults, this should be a Map, with possible keys
     *               defaultTemplate, defaultTopic (for list creation) and
     *               defaultQuiet (for adding or removing users from lists).
     *               If present, those will replace the default values, respectively:
     *               - defaultTemplate: private_working_group
     *               - defaultTopic: computing
     *               - defaultQuiet: 1 (quiet mode)
     * @return a Map containing error and errorType, and result.
     */
    public Map init(Map params) {
        Map result = new HashMap();
        String resultstring;
        //handle parameters
        if (params.containsKey(PARAM_SYMPASOAPURL)) {
            sympasoapUrl = (String) params.get(PARAM_SYMPASOAPURL);
        } else {
            result.put(ERRORTYPE, ERRORWRAPPER);
            result.put(ERROR, PARAMERR_SYMPASOAPURL);
            return result;
        }
        if (params.containsKey(PARAM_TRUSTEDAPP)) {
            trustedApp = (String) params.get(PARAM_TRUSTEDAPP);
        } else {
            result.put(ERRORTYPE, ERRORWRAPPER);
            result.put(ERROR, PARAMERR_TRUSTEDAPP);
            return result;
        }
        if (params.containsKey(PARAM_APPPASSWORD)) {
            appPwd = (String) params.get(PARAM_APPPASSWORD);
        } else {
            result.put(ERRORTYPE, ERRORWRAPPER);
            result.put(ERROR, PARAMERR_APPPASSWORD);
            return result;
        }
        resultstring = "Initialisation done.";
        //handle optional parameters
        if (params.containsKey("defaults")) {
            resultstring += " Defaults set: ";
            boolean nodefaults = true;
            Object thedefaults = params.get("defaults");
            //need to check that defaults is a Map
            if (thedefaults instanceof Map) {
                Map defaults = (Map) thedefaults;
                if (defaults.containsKey(PARAM_DEF_TEMPLATE)) {
                    defaultTemplate = (String) defaults.get(PARAM_DEF_TEMPLATE);
                    resultstring += " " + PARAM_DEF_TEMPLATE;
                    nodefaults = false;
                }
                if (defaults.containsKey(PARAM_DEF_QUIET)) {
                    defaultQuiet = (String) defaults.get(PARAM_DEF_QUIET);
                    resultstring += " " + PARAM_DEF_QUIET;
                    nodefaults = false;
                }
                if (defaults.containsKey(PARAM_DEF_TOPIC)) {
                    defaultTopic = (String) defaults.get(PARAM_DEF_TOPIC);
                    resultstring += " " + PARAM_DEF_TOPIC;
                    nodefaults = false;
                }
            } else {
                nodefaults = true;
            }
            if (nodefaults) {
                resultstring += "none";
            }
        }
        result.put(RESULT, resultstring);
        result.put(ERROR, null);
        return result;
    }

    /**
     * Get the mailing lists to which the user is a subscriber, moderator or owner.
     *
     * @param user The user for which to test the list membership.
     * @return A Map containing error and errorType, and result which is a list of maps.
     * Each map contains items for a given list:
     * - isSubscriber (0 or 1)
     * - isOwner (0 or 1)
     * - isEditor (0 or 1) - whether the user is a moderator of the list
     * - subject (the list subject)
     * - listAddress (e.g. my-list@mydoma.in)
     * - name (e.g. my-list)
     * - homepage (web URL for the list)
     */
    public Map getUserLists(String user) {
        return call("which", user, new String[]{});
    }

    /**
     * Get a list of all mailing lists
     *
     * @param user The sympa user which runs the command.
     *             Note that different users could view different lists. A sympa admin would view all lists.
     * @return A Map containing error and errorType, and result which is a list of maps.
     * Each map contains items for a given list:
     * - subject (the list subject)
     * - listAddress (e.g. my-list@mydoma.in)
     * - name (e.g. my-list)
     * - homepage (web URL for the list)
     */
    public Map getLists(String user) {
        return call("lists", user, new String[]{});
    }

    /**
     * Get information about a mailing list for a given user.
     *
     * @param params A Map containing two entries:
     *               userEmail: the user on behalf of which the Sympa info command is executed
     *               listName: the name of the mailing list for which to retrieve information
     * @return A Map containing error and errorType, and result which is a list of maps.
     * Each map contains items for a given list:
     * - isSubscriber (0 or 1)
     * - isOwner (0 or 1)
     * - isEditor (0 or 1) - whether the user is a moderator of the list
     * - subject (the list subject)
     * - listAddress (e.g. my-list@mydoma.in)
     * - homepage (web URL for the list)
     */
    public Map getInfo(Map params) {
        if (params.containsKey(PARAM_USEREMAIL)) {
            if (params.containsKey(PARAM_LISTNAME)) {
                return call("info", (String) params.get(PARAM_USEREMAIL),
                        new String[]{(String) params.get(PARAM_LISTNAME)});
            } else {
                //return a wrapper error, missing parameter "listName"
                return errorWrapperMap(PARAMERR_LISTNAME);
            }

        } else {
            //return a wrapper error, missing parameter "userEmail"
            return errorWrapperMap(PARAMERR_USEREMAIL);
        }
    }

    /**
     * Get the subscribers list of a mailing list for a given user.
     *
     * @param params A Map containing two entries:
     *               userEmail: the user on behalf of which the Sympa review command is executed
     *               listName: the name of the mailing list for which to retrieve information
     * @return A Map containing error and errorType, and result which is an array of subscribers.
     */
    public Map getListSubscribers(Map params) {
        if (params.containsKey(PARAM_USEREMAIL)) {
            if (params.containsKey(PARAM_LISTNAME)) {
                return call("review", (String) params.get(PARAM_USEREMAIL),
                        new String[]{(String) params.get(PARAM_LISTNAME)});
            } else {
                //return a wrapper error, missing parameter "listName"
                return errorWrapperMap(PARAMERR_LISTNAME);
            }

        } else {
            //return a wrapper error, missing parameter "userEmail"
            return errorWrapperMap(PARAMERR_USEREMAIL);
        }
    }

    /**
     * Create a mailing list.
     *
     * @param params A Map containing:
     *               - userEmail: the user requesting the list creation (mandatory)
     *               Note: if the user is a listmaster, the list will be activated,
     *               otherwise it will be pending.
     *               - listName: the name of the list to be created (mandatory)
     *               - subject: the subject of the list (mandatory)
     *               - description: the description of the list (mandatory)
     *               - topic: the topic of the list (optional, otherwise defaultTopic will be used)
     *               - template: the template for the list (optional, otherwise defaultTemplate will be used)
     * @return A Map containing error and errorType, and result which is either true or false
     */
    public Map createList(Map params) {
        if (params.containsKey(PARAM_USEREMAIL)) {
            if (params.containsKey(PARAM_LISTNAME)) {
                if (params.containsKey(PARAM_SUBJECT)) {
                    if (params.containsKey(PARAM_DESCRIPTION)) {
                        //check optional topic and template parameters
                        //otherwise use defaults
                        return call("createList", (String) params.get(PARAM_USEREMAIL),
                                new String[]{(String) params.get(PARAM_LISTNAME),
                                        (String) params.get(PARAM_SUBJECT),
                                        (params.containsKey(PARAM_TEMPLATE)) ?
                                                (String) params.get(PARAM_TEMPLATE) : defaultTemplate,
                                        (String) params.get(PARAM_DESCRIPTION),
                                        (params.containsKey(PARAM_TOPIC)) ?
                                                (String) params.get(PARAM_TOPIC) : defaultTopic});
                    } else {
                        //return a wrapper error, missing parameter "description"
                        return errorWrapperMap(PARAMERR_DESCRIPTION);
                    }
                } else {
                    //return a wrapper error, missing parameter "subject"
                    return errorWrapperMap(PARAMERR_SUBJECT);
                }
            } else {
                //return a wrapper error, missing parameter "listName"
                return errorWrapperMap(PARAMERR_LISTNAME);
            }

        } else {
            //return a wrapper error, missing parameter "userEmail"
            return errorWrapperMap(PARAMERR_USEREMAIL);
        }
    }

    /**
     * Add a user to a mailing list.
     *
     * @param params A Map containing:
     *               - userEmail: the user email to add as a list subscriber (mandatory)
     *               - sympaUserEmail: the user performing the request (mandatory)
     *               - listName: the name of the list to be created (mandatory)
     *               - userFullName (optional): the full name of the user. If not present, "" will be used.
     *               - quiet (optional): whether the subscription should be quiet or not.
     *               If not present, the defaultQuiet will be used.
     * @return A Map containing error and errorType, and result which is either true or false
     */
    public Map addUserToList(Map params) {
        if (params.containsKey(PARAM_USEREMAIL)) {
            if (params.containsKey(PARAM_LISTNAME)) {
                if (params.containsKey(PARAM_SYMPAUSEREMAIL)) {
                    //check optional topic and template parameters
                    //otherwise use defaults
                    return call("add", (String) params.get(PARAM_SYMPAUSEREMAIL),
                            new String[]{(String) params.get(PARAM_LISTNAME),
                                    (String) params.get(PARAM_USEREMAIL),
                                    (params.containsKey(PARAM_USERFULLNAME)) ?
                                            (String) params.get(PARAM_USERFULLNAME) : "",
                                    (params.containsKey(PARAM_QUIET)) ?
                                            (String) params.get(PARAM_QUIET) : defaultQuiet});
                } else {
                    //return a wrapper error, missing parameter "sympaUserEmail"
                    return errorWrapperMap(PARAMERR_SYMPAUSEREMAIL);
                }
            } else {
                //return a wrapper error, missing parameter "listName"
                return errorWrapperMap(PARAMERR_LISTNAME);
            }

        } else {
            //return a wrapper error, missing parameter "userEmail"
            return errorWrapperMap(PARAMERR_USEREMAIL);
        }
    }

    /**
     * Remove a user from a mailing list.
     *
     * @param params A Map containing:
     *               - userEmail: the user email to add as a list subscriber (mandatory)
     *               - sympaUserEmail: the user performing the request (mandatory)
     *               - listName: the name of the list to be created (mandatory)
     *               - quiet (optional): whether the subscription should be quiet or not.
     *               If not present, the defaultQuiet will be used.
     * @return A Map containing error and errorType, and result which is either true or false
     */
    public Map removeUserFromList(Map params) {
        if (params.containsKey(PARAM_USEREMAIL)) {
            if (params.containsKey(PARAM_LISTNAME)) {
                if (params.containsKey(PARAM_SYMPAUSEREMAIL)) {
                    //check optional topic and template parameters
                    //otherwise use defaults
                    return call("del", (String) params.get(PARAM_SYMPAUSEREMAIL),
                            new String[]{(String) params.get(PARAM_LISTNAME),
                                    (String) params.get(PARAM_USEREMAIL),
                                    (params.containsKey(PARAM_QUIET)) ?
                                            (String) params.get(PARAM_QUIET) : defaultQuiet});
                } else {
                    //return a wrapper error, missing parameter "sympaUserEmail"
                    return errorWrapperMap(PARAMERR_SYMPAUSEREMAIL);
                }
            } else {
                //return a wrapper error, missing parameter "listName"
                return errorWrapperMap(PARAMERR_LISTNAME);
            }

        } else {
            //return a wrapper error, missing parameter "userEmail"
            return errorWrapperMap(PARAMERR_USEREMAIL);
        }
    }

    /**
     * Get information about a mailing list for a given user.
     *
     * @param params A Map containing two entries:
     *               userEmail: the user on behalf of which the Sympa info command is executed
     *               listName: the name of the mailing list for which to retrieve information
     *               role: the role to check (valid roles are subscriber, editor and owner)
     * @return A Map containing error and errorType, and result which is either true or false
     */
    public Map amI(Map params) {
        if (params.containsKey(PARAM_USEREMAIL)) {
            if (params.containsKey(PARAM_LISTNAME)) {
                if (params.containsKey(PARAM_ROLE)) {
                    return call("amI", null, new String[]{(String) params.get(PARAM_LISTNAME),
                            (String) params.get(PARAM_ROLE), (String) params.get(PARAM_USEREMAIL)});
                } else {
                    //return a wrapper error, missing parameter "role"
                    return errorWrapperMap(PARAMERR_ROLE);
                }
            } else {
                //return a wrapper error, missing parameter "listName"
                return errorWrapperMap(PARAMERR_LISTNAME);
            }
        } else {
            //return a wrapper error, missing parameter "userEmail"
            return errorWrapperMap(PARAMERR_USEREMAIL);
        }
    }

    /**
     * Generic call method to construct the Sympa SOAP call and obtain the result
     *
     * @param method The Sympa method to call
     * @param user The user for which the call is executed
     * @param params A possibly empty list of String parameters
     * @return a Map containing the raw XML result as a String from the SOAP call
     * and an error it any encountered
     */
    public Map call(String method, String user, String[] params) {
        HashMap result = new HashMap();
        String error = null;
        System.out.println("Sympa wrapper call: " + method);
        try {
            int paramsnum = params.length;
            //construct the envelope
            String soapEnvelope = soapEnvStart + trustedApp + soapEnvAppToPwd + decrypt(appPwd) +
                    soapEnvPwdToUserEmail + user + soapEnvUserEmailToMethod + method +
                    soapEnvBeforeParam + paramsnum;

            if (params.length == 0) {
                //distinguish between method with a param (e.g. which) and ones without param (e.g. info)
                soapEnvelope = soapEnvelope + soapEnvNoParam;
            } else {
                soapEnvelope = soapEnvelope + soapEnvWithParam;
                //add an item for each parameter
                for (int i = 0; i < params.length; i++) {
                    soapEnvelope = soapEnvelope + itemStart + params[i] + itemEnd;
                }
                soapEnvelope = soapEnvelope + soapEnvAfterParam;
            }
            soapEnvelope = soapEnvelope + soapEnvEnd;
            //System.out.println("Envelope sent: " + soapEnvelope);
            //do the call
            Document doc = SympaSoapClient.soapCall(sympasoapUrl, soapAction, soapEnvelope);
            System.out.println("Printing result");
            //printDoc(doc);
            System.out.println(getStringFromDocument(doc));
            //System.out.println("End result");
            //extracting the SOAP response node from the document
            Node responseNode = doc.getFirstChild().getFirstChild().getFirstChild();
            String responseName = responseNode.getNodeName();
            //System.out.println("Node name: " + responseName);
            switch (responseName) {
                case "soap:Fault": //Soap returned an error
                    result.put(ERRORTYPE, ERRORSYMPA);
                    NodeList soapnodes = responseNode.getChildNodes();
                    String[] errors = new String[soapnodes.getLength()];
                    for (int i = 0; i < soapnodes.getLength(); i++) {
                        //System.out.println("Fault item: " + soapnodes.item(i).getNodeName() + " : " + soapnodes.item(i).getTextContent());
                        errors[i] = soapnodes.item(i).getNodeName() + ": " + soapnodes.item(i).getTextContent();
                    }
                    error = Arrays.toString(errors);
                    break;
                case "authenticateRemoteAppAndRunResponse": //the call went fine
                    switch (method) {
                        case "which":
                        case "lists":
                            NodeList nList = doc.getElementsByTagName("item");
                            //build an Array of Maps
                            HashMap[] thelists = new HashMap[nList.getLength()];
                            //extract list information
                            for (int temp = 0; temp < nList.getLength(); temp++) {
                                Node nNode = nList.item(temp);
                                String fulllist = nNode.getFirstChild().getTextContent();
                                //System.out.println("List : " + fulllist);
                                HashMap onelist = new HashMap();
                                String[] listitems = StringUtils.split(fulllist, ";");
                                for (int item = 0; item < listitems.length; item++) {
                                    String key = StringUtils.substringBefore(listitems[item], "=");
                                    String value = StringUtils.substringAfter(listitems[item], "=");
                                    //System.out.println("Item " + key + "->" + value);
                                    if (key.equals("listAddress")) {
                                        onelist.put("name", StringUtils.substringBefore(value, "@"));
                                    }
                                    onelist.put(key, value);
                                }
                                thelists[temp] = onelist;
                                //System.out.println("List: " + onelist.toString());
                            }
                            result.put(RESULT, thelists);
                            break;
                        case "review": //list subscribers
                            NodeList nSubs = doc.getElementsByTagName("item");
                            //build an Array for subscribers
                            String[] thesubs = new String[nSubs.getLength()];
                            //extract list of subscribers
                            for (int temp = 0; temp < nSubs.getLength(); temp++) {
                                Node nNode = nSubs.item(temp);
                                String subscriber = nNode.getFirstChild().getTextContent();
                                //System.out.println("Subscriber : " + subscriber);
                                thesubs[temp] = subscriber;
                            }
                            //System.out.println("List: " + Arrays.toString(thesubs));
                            result.put(RESULT, thesubs);
                            break;
                        case "info": //info about a given list
                            nList = doc.getElementsByTagName("item").item(0).getChildNodes();
                            //build an Array of Maps
                            HashMap[] listinfoarray = new HashMap[nList.getLength()];
                            //extract list information
                            for (int temp = 0; temp < nList.getLength(); temp++) {
                                Node nNode = nList.item(temp);
                                //System.out.println("\nCurrent Element :" + nNode.getNodeName());
                                String listitem = nNode.getFirstChild().getTextContent();
                                String listitemname = nNode.getNodeName();
                                //System.out.println("List item : " + listitemname + "=" + listitem);
                                HashMap onelist = new HashMap();
                                //String address = fulllist.substring(fulllist.indexOf("listAddress=")+12);
                                onelist.put(listitemname, listitem);
                                listinfoarray[temp] = onelist;
                                //System.out.println("List: " + onelist.toString());
                            }
                            result.put(RESULT, listinfoarray);
                            break;
                        case "createList":
                        case "amI":
                        case "add":
                        case "del"://result should be true or 1 or ""
                            //call was OK, just return true
                            //nList = doc.getElementsByTagName("result").item(0).getChildNodes();
                            nList = doc.getElementsByTagName("authenticateRemoteAppAndRunResponse").item(0).getChildNodes();
                            //System.out.println("Content of first child: " + nList.item(0).getTextContent());
                            result.put(RESULT, nList.item(0).getTextContent());
                            break;
                        default:
                            result.put(ERRORTYPE, ERRORWRAPPER);
                            error = "Method \"" + method + "\" is not supported by this wrapper";
                    }
                    break;
                default:
                    result.put(ERRORTYPE, ERRORWRAPPER);
                    error = "SOAP return could not be interpreted: " + responseName;
            }
        } catch (Exception e) {
            e.printStackTrace();
            error = e.toString();
            result.put(ERRORTYPE, ERRORWRAPPER);
        }
        result.put(ERROR, error);
        return result;
    }

    /**
     * Return a map containing ERRORTYPE key with ERRORWRAPPER, and ERROR keys with the error.
     *
     * @param error to be put as value of key ERROR
     * @return The Map
     */
    private Map errorWrapperMap(String error) {
        HashMap result = new HashMap();
        result.put(ERRORTYPE, ERRORWRAPPER);
        result.put(ERROR, error);
        return result;
    }

    /**
     * Utility for printing an XML response.
     *
     * @param response The raw XML response to be printed
     */
    private void printXMLItems(String response) {
        try {
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(response)));
            Node envelope = doc.getFirstChild();
            Node body = envelope.getFirstChild();
            Node soapresponse = body.getFirstChild();
            Node listinfo = soapresponse.getFirstChild();
            NodeList nList = doc.getElementsByTagName("item");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                String theItem = nNode.getFirstChild().getTextContent();
                System.out.println("Item:");
                System.out.println(theItem);
                //String address = fulllist.substring(fulllist.indexOf("listAddress=") + 12);
                //System.out.println(address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void printDoc(Document doc) {
        Node envelope = doc.getFirstChild();
        Node body = envelope.getFirstChild();
        Node soapresponse = body.getFirstChild();
        Node listinfo = soapresponse.getFirstChild();
        NodeList nList = doc.getElementsByTagName("item");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            System.out.println("\nCurrent Element :" + nNode.getNodeName());

            String theItem = nNode.getFirstChild().getTextContent();
            System.out.println("Item:");
            System.out.println(theItem);
            //String address = fulllist.substring(fulllist.indexOf("listAddress=") + 12);
            //System.out.println(address);
        }
    }

    private String getStringFromDocument(Document doc)
    {
        try
        {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
        }
        catch(TransformerException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Encrypt a String
     *
     * @param input : the String to be encrypted
     * @return the encrypted String
     */
    @Override
    public String encrypt(String input) {
        //System.out.println("Input for encoding is: " + input);
        try {
            byte[] decodedKey = Base64.decode(md5);
            Cipher cipher = Cipher.getInstance("AES");
            // rebuild key using SecretKeySpec
            SecretKey originalKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, originalKey);
            byte[] cipherText = cipher.doFinal(input.getBytes("UTF-8"));
            return Base64.encode(cipherText).toString();
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error occured while encrypting data", e);
        }
    }

    /**
     * Decrypt a String
     *
     * @param input : the String to be decrypted
     * @return the decrypted String
     */
    @Override
    public String decrypt(String input) {
        //System.out.println("Input for decoding is: " + input);
        try {
            byte[] decodedKey = Base64.decode(md5);
            Cipher cipher = Cipher.getInstance("AES");
            // rebuild key using SecretKeySpec
            SecretKey originalKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            cipher.init(Cipher.DECRYPT_MODE, originalKey);
            byte[] cipherText = cipher.doFinal(Base64.decode(input));
            return new String(cipherText);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error occured while decrypting data", e);
        }
    }
}

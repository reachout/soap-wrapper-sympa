/**
 This file is part of Sympa SOAP service Component.

 Sympa SOAP service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Sympa SOAP is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Sympa SOAP (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved

 @author Alexandre Lefebvre
 */
package org.ow2.sympa;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.sympa.internal.SympaWrapperImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Tests for the {@link SympaWrapper} component.
 */

public class SympaWrapperTest {

    private String user = "list.master@org.org"; //the default listmaster user
    private SympaWrapperImpl sympawrapper;

    {
        Map init = new HashMap();
        /**
         * For local tests
         */
        init.put("sympaSoapUrl", "http://10.1.2.199/sympasoap");
        init.put("trustedApp", "appname");
        init.put("appPassword", "kcGpWi+196RnKAokODYRofl9/CiInmNc70BiOB0pDOo=");
        //init.put("appPassword", "4YZVpBk68Px1mqGwOnA/Hw==");


        /**
         * For remote testing
         */
        //init.put("sympaSoapUrl", "https://my.sympa.org/sympasoap");
        //init.put("trustedApp", "appname");
        //init.put("appPassword", "encryptedpassword");

        sympawrapper = new SympaWrapperImpl();
        sympawrapper.init(init);
    }

    @Test
    public void testInit() throws Exception {
        Map init = new HashMap();
        init.put("sympaSoapUrl", "http://10.1.2.199/sympasoap");
        init.put("trustedApp", "appname");
        init.put("appPassword", "4YZVpBk68Px1mqGwOnA/Hw==");
        sympawrapper = new SympaWrapperImpl();
        Map result = sympawrapper.init(init);
        Assert.assertEquals("Initialisation done.", result.get("result"));
    }

    @Test
    public void testInitWrongDefaults() throws Exception {
        Map init = new HashMap();
        init.put("sympaSoapUrl", "http://10.1.2.199/sympasoap");
        init.put("trustedApp", "appname");
        init.put("appPassword", "4YZVpBk68Px1mqGwOnA/Hw==");
        Map defaults = new HashMap();
        defaults.put("bla", "bla");
        init.put("defaults", defaults);
        sympawrapper = new SympaWrapperImpl();
        Map result = sympawrapper.init(init);
        Assert.assertEquals("Initialisation done. Defaults set: none", result.get("result"));
    }

    @Test
    public void testInitDefaultsAsString() throws Exception {
        Map init = new HashMap();
        init.put("sympaSoapUrl", "http://10.1.2.199/sympasoap");
        init.put("trustedApp", "appname");
        init.put("appPassword", "4YZVpBk68Px1mqGwOnA/Hw==");
        init.put("defaults", "hello");
        sympawrapper = new SympaWrapperImpl();
        Map result = sympawrapper.init(init);
        Assert.assertEquals("Initialisation done. Defaults set: none", result.get("result"));
    }

    @Test
    public void testWhichCall() throws Exception {
        Map result = sympawrapper.call("which", user, new String[] {});
        System.out.println("Error: " + result.get("error"));
        System.out.println("Keys: " + ((HashMap[]) result.get("result"))[0].keySet().toString());
        Assert.assertEquals(null, result.get("error"));
        Assert.assertTrue(((HashMap[]) result.get("result"))[0].get("name") != null);
    }

    @Test
    public void testWhich() throws Exception {
        Map result = sympawrapper.getUserLists(user);
        System.out.println("Result: " + result.get("result"));
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testWhichOtherUser() throws Exception {
        Map result = sympawrapper.getUserLists("first.last@domain.com");
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testListsCall() throws Exception {
        Map result = sympawrapper.call("lists", user, new String[] {});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testListsCallToto() throws Exception {
        Map result = sympawrapper.call("lists", "toto", new String[] {});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testLists() throws Exception {
        Map result = sympawrapper.getLists(user);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testListsAnon() throws Exception {
        Map result = sympawrapper.getLists("dummyuser");
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testListsUSS() throws Exception {
        Map result = sympawrapper.getLists("first.last@domain.com");
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testInfo() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", user);
        params.put("listName", "test-list");
        Map result = sympawrapper.getInfo(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testInfoNonExistingList() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", user);
        params.put("listName", "not-a-list");
        Map result = sympawrapper.getInfo(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("[faultcode: soap:Server, faultstring: Unknown list, detail: List not-a-list unknown]", result.get("error"));
        Assert.assertEquals("ErrorSympa", result.get("errorType"));
    }

    @Test
    public void testInfoMissingUserEmail() throws Exception {
        Map params = new HashMap();
        params.put("user", user);
        params.put("listName", "test-list");
        Map result = sympawrapper.getInfo(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("Missing entry for userEmail in parameters", result.get("error"));
        Assert.assertEquals("ErrorWrapper", result.get("errorType"));
    }

    @Test
    public void testInfoMissingListName() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", user);
        params.put("list", "test-list");
        Map result = sympawrapper.getInfo(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("Missing entry for listName in parameters", result.get("error"));
        Assert.assertEquals("ErrorWrapper", result.get("errorType"));
    }

    @Test
    public void testInfoWrongParams() throws Exception {
        Map params = new HashMap();
        params.put("hello", 1);
        Map result = sympawrapper.getInfo(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("Missing entry for userEmail in parameters", result.get("error"));
        Assert.assertEquals("ErrorWrapper", result.get("errorType"));
    }

    @Test
    public void testInfoCall() throws Exception {
        Map result = sympawrapper.call("info", user, new String[] {"test-list"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testInfoUSSCall() throws Exception {
        Map result = sympawrapper.call("info", "first.last@domain.com", new String[] {"test-list"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testamICallSub() throws Exception {
        Map result = sympawrapper.call("amI", null, new String[] {"test-list","subscriber","first.last@domain.com"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("true", result.get("result"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testamICallEdit() throws Exception {
        Map result = sympawrapper.call("amI", null, new String[] {"test-list","editor","first.last@domain.com"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("false", result.get("result"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testamISub() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", user);
        params.put("listName", "test-list");
        params.put("role", "subscriber");
        Map result = sympawrapper.amI(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("true", result.get("result"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testamICallOwn() throws Exception {
        Map result = sympawrapper.call("amI", null, new String[] {"test-list","owner","first.last@domain.com"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("false", result.get("result"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testamICallDummy() throws Exception {
        Map result = sympawrapper.call("amI", null, new String[] {"test-list","dummy","first.last@domain.com"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("ErrorSympa", result.get("errorType"));
        Assert.assertEquals("[faultcode: soap:Server, faultstring: Unknown function., detail: Function dummy unknown]", result.get("error"));
    }

    @Test
    public void testReviewCall() throws Exception {
        Map result = sympawrapper.call("review", user, new String[] {"test-list"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testReview() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", user);
        params.put("listName", "test-list");
        Map result = sympawrapper.getListSubscribers(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals(null, result.get("errorType"));
    }

    @Test
    public void testReviewUnauthorizedUser() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", "first.last@domain.com");
        params.put("listName", "test-list");
        Map result = sympawrapper.getListSubscribers(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("[faultcode: soap:Server, faultstring: Not allowed, detail: The 'review' feature is restricted to list owners.]", result.get("error"));
        Assert.assertEquals("ErrorSympa", result.get("errorType"));
    }

    @Test
    public void testCreateListCall() throws Exception {
        Map result = sympawrapper.call("createList", user, new String[] {"test-list2","Test1 list subject",
                "private_working_group","Test1 list description","MyTopic"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("true", result.get("result"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testCreateList() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", user);
        params.put("listName", "test-list123");
        params.put("subject", "Test subject for list 123");
        params.put("description", "Test description for list 123");
        Map result = sympawrapper.createList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("true", result.get("result"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testCreateListPlusPlus() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", user);
        params.put("listName", "test-list456");
        params.put("subject", "Test subject for list 456");
        params.put("description", "Test description for list 456");
        params.put("topic", "reachout");
        params.put("template", "discussion_list");
        Map result = sympawrapper.createList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals("true", result.get("result"));
        Assert.assertEquals(null, result.get("error"));
    }

    @Test
    public void testAddUserCall() throws Exception {
        Map result = sympawrapper.call("add", user, new String[] {"test-list2", "user.name@domain.com", "User Name", "1"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals("true", result.get("result"));
    }

    @Test
    public void testAddUser() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", "user.name@domain.com");
        params.put("listName", "test-list");
        params.put("userFullName", "User Name");
        params.put("quiet", "1");
        params.put("sympaUserEmail", user);
        Map result = sympawrapper.addUserToList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals(null, result.get("errorType"));
        Assert.assertEquals("true", result.get("result"));
    }

    @Test
    public void testAddAlexOther() throws Exception {
        user = "list.owner@domain.com";
        Map params = new HashMap();
        params.put("userEmail", "user.name@domain.com");
        params.put("listName", "test-list");
        params.put("userFullName", "User Name");
        params.put("quiet", "1");
        params.put("sympaUserEmail", user);
        Map result = sympawrapper.addUserToList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals(null, result.get("errorType"));
        Assert.assertEquals("", result.get("result"));
    }

    @Test
    public void testDelAlexOther() throws Exception {
        user = "list.owner@domain.com";
        Map params = new HashMap();
        params.put("userEmail", "user.name@domain.com");
        params.put("listName", "test-list");
        params.put("quiet", "1");
        params.put("sympaUserEmail", user);
        Map result = sympawrapper.removeUserFromList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals(null, result.get("errorType"));
        Assert.assertEquals("1", result.get("result"));
    }

    @Test
    public void testAddUserNoOptions() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", "first.last@domain.com");
        params.put("listName", "test-list");
        params.put("sympaUserEmail", user);
        Map result = sympawrapper.addUserToList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals("true", result.get("result"));
    }

    @Test
    public void testDelUserCall() throws Exception {
        Map result = sympawrapper.call("del", user, new String[] {"test-list", "first.last@domain.com", "1"});
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals("true", result.get("result"));
    }

    @Test
    public void testDelUser() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", "first.last@domain.com");
        params.put("listName", "test-list");
        params.put("quiet", "1");
        params.put("sympaUserEmail", user);
        Map result = sympawrapper.removeUserFromList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals(null, result.get("errorType"));
        Assert.assertEquals("true", result.get("result"));
    }

    @Test
    public void testDelUserNoDefault() throws Exception {
        Map params = new HashMap();
        params.put("userEmail", "first.last@domain.com");
        params.put("listName", "test-list");
        params.put("sympaUserEmail", user);
        Map result = sympawrapper.removeUserFromList(params);
        System.out.println("Error: " + result.get("error"));
        Assert.assertEquals(null, result.get("error"));
        Assert.assertEquals("true", result.get("result"));
    }

    @Test
    public void testEncryptDecrypt() throws Exception {
        System.out.println("testEncryptDecrypt");
        String input = "sympasoapwd";
        String encrypted = sympawrapper.encrypt(input);
        System.out.println("Encrypted is " + encrypted);

        String decrypted = sympawrapper.decrypt(encrypted);
        System.out.println("Decrypted is " + decrypted);

        decrypted = sympawrapper.decrypt("4YZVpBk68Px1mqGwOnA/Hw==");
        System.out.println("Decrypted 2 is " + decrypted);

        Assert.assertEquals(input, decrypted);
    }

    @Test
    public void testEncryptDecryptOW2() throws Exception {
        System.out.println("testEncryptDecrypt");
        String input = "thisIsAPassword";
        String encrypted = sympawrapper.encrypt(input);
        System.out.println("Encrypted is " + encrypted);

        String decrypted = sympawrapper.decrypt(encrypted);
        System.out.println("Decrypted is " + decrypted);

        decrypted = sympawrapper.decrypt("decryptedpassword");
        System.out.println("Decrypted is " + decrypted);

        Assert.assertEquals(input, decrypted);
    }
}

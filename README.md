# Sympa SOAP wrapper

An XWiki component to manage Sympa mailing lists using Sympa SOAP

## Summary

This component exposes an XWiki service with methods to manipulate Sympa mailing lists for
an installed Sympa instance.

Authentication is done through a trusted Sympa application, which must have 
been configured on the Sympa server.

See the Sympa SOAP documentation in <https://sympa-community.github.io/manual/customize/soap-api.html>

## Documentation

A generated Javadoc can be found [here](https://reachout.ow2.io/soap-wrapper-sympa/). 

## Getting started

The prerequisites are:
- Java Development Kit JDK8 or higher
- Apache Maven

### Installation

Run `mvn package` in the root of the project folder. This will build a `sympa-wrapper-x.x.xx-SNAPSHOT.jar` in `target` ready for usage as an XWiki ScriptService.

### Deployment

Deployment requires authorization. You need to put your valid OW2 credentials into Maven's `settings.xml` file like here:

```
<settings>
  <servers>
    <server>
      <id>ow2.snapshot</id>
      <username>USERNAME</username>
      <password>PASS</password>
    </server>
  </servers>
</settings>
```

There are two locations where a settings.xml file may live:

- The Maven install: `${maven.home}/conf/settings.xml`
- A user’s install: `${user.home}/.m2/settings.xml`

For more information, we refer to the [settings reference](https://maven.apache.org/settings.html).

Once done, the deployment process can be started. 
After updating the product version in `pom.xml`, run `mvn deploy`. 
This will deploy the binary to the XWiki repository specified 
inside `pom.xml`. 

Once done, head to the XWiki admin page, 
go to `'Extensions'->'Advanced search'`. 
Search for the id `org.ow2:sympa-wrapper` with the current product 
version specified in `pom.xml`. 
Remove this extension and install the new deployed version. 

**Important:** do not attempt to install the new version by 
updating, this will somehow break XWiki. 
Please reinstall the extension by removing and installing.

## Usage (velocity)

This section covers the commonly used API methods. 
Please visit the full [documentation](https://reachout.ow2.io/soap-wrapper-sympa/) 
 for more details.

### Initializate the API

Initializing this Sympa component requires calling the init method with:
- the Sympa SOAP URL (key sympaSoapUrl)
- the trusted application name (key trustedApp)
- the trusted application encrypted password (key trustedAppPassword) (this can be done with the crypt component)

Additional optional parameters include:
- the default Sympa template used when creating a mailing list 
(key defaultTemplate). If not specified, the default is private_working_group
- the default Sympa topic used when creating a mailing list
(key defaultTopic). If not specified, the default is computing
- the default mode when adding or removing users from list 
(key defaultQuiet). If not specified, the default is 1 (quiet).

## License

 Sympa SOAP service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Sympa SOAP is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Sympa SOAP (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved